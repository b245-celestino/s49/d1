fetch("https://jsonplaceholder.typicode.com/posts")
  .then((response) => response.json())
  .then((result) => {
    console.log(result);
    showPosts(result);
  });

// show post
// we are going to create a function that will let us show the posts from our API

const showPosts = (posts) => {
  let entries = ``;

  posts.forEach((post) => {
    entries += `
    <div id = "post-${post.id}"> 
        <h3 id = "post-title-${post.id}">${post.title}</h3>
        <p id = "post-body-${post.id}">${post.body}</p>
        <button onclick = "editPost(${post.id})">Edit</button>
        <button onclick = "deletePost(${post.id})">Delete</button>
    
    </div>`;
  });

  document.querySelector("#div-post-entries").innerHTML = entries;
};

// Post data on our API

document.querySelector("#form-add-post").addEventListener("submit", (event) => {
  event.preventDefault();

  fetch("https://jsonplaceholder.typicode.com/posts", {
    method: "POST",
    body: JSON.stringify({
      title: document.querySelector("#txt-title").value,
      body: document.querySelector("#txt-body").value,
      userId: 1,
    }),
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => response.json())
    .then((result) => {
      console.log(result);

      document.querySelector("#txt-title").value = null;
      document.querySelector("#txt-body").value = null;
      alert("Post is successfully added!");
    });
});

// Edit Post

const editPost = (id) => {
  console.log(id);
  let title = document.querySelector(`#post-title-${id}`).innerHTML;
  let body = document.querySelector(`#post-body-${id}`).innerHTML;

  console.log(title);
  console.log(body);

  document.querySelector("#txt-edit-title").value = title;
  document.querySelector("#txt-edit-body").value = body;
  document.querySelector("#txt-edit-id").value = id;

  document.querySelector("#btn-submit-update").removeAttribute("disabled");
};

document
  .querySelector("#form-edit-post")
  .addEventListener("submit", (event) => {
    event.preventDefault();

    let id = document.querySelector("#txt-edit-id").value;

    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
      method: "PUT",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        title: document.querySelector("#txt-edit-title").value,
        id: id,
        body: document.querySelector("#txt-edit-body").value,
        userId: 1,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        console.log(result);
        alert("The post is successfully updated!");
        document.querySelector("#txt-edit-title").value = null;
        document.querySelector("#txt-edit-body").value = null;
        document.querySelector("#txt-edit-id").value = null;

        document
          .querySelector("#btn-submit-update")
          .setAttribute("disabled", true);
      });
  });

// delete post

const deletePost = (id) => {
  fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => {
      return response.json();
    })
    .then((result) => {
      console.log(result);
    });
  document.querySelector(`#post-${id}`).remove();
};
